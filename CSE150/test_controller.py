# Lab3 Skeleton

from pox.core import core
#from ipaddr import IPNetwork, IPAddress

import pox.openflow.libopenflow_01 as of

log = core.getLogger()

class Routing (object):
  """
  A Firewall object is created for each switch that connects.
  A Connection object for that switch is passed to the __init__ function.
  """
  def __init__ (self, connection):
    # Keep track of the connection to the switch so that we can
    # send it messages!
    self.connection = connection

    # This binds our PacketIn event listener
    connection.addListeners(self)

  def do_routing (self, packet, packet_in, port_on_switch, switch_id):
    # port_on_swtich - the port on which this packet was received
    # switch_id - the switch which received this packet

    # Your code here
    tcp = packet.find("tcp")
    icmp = packet.find("icmp")
    udp = packet.find("udp")

    sales_subnet = {'Laptop1': '200.20.2.8', 'Printer': '200.20.2.10', 'Laptop2': '200.20.2.9'}
    ot_subnet = {'Workstation1': '200.20.3.4', 'Workstation2': '200.20.3.5'}
    it_subnet = {'Workstation3': '200.20.4.7', 'Workstation4': '200.20.4.6'}
    data_subnet = {'Server2': '200.20.1.1', 'WebServer': '200.20.1.2', 'DNSserver': '200.20.1.3'}

    
    if icmp:
      ipv4 = packet.find("ipv4")
      if switch_id == 5:
        print("SWITCH 5")
        print(ipv4.srcip)
        print(ipv4.dstip)
        if ipv4.srcip in sales_subnet.values() and ipv4.dstip in ot_subnet.values():
          port = 2
          print("going to switch 2")
          self.accept(packet, packet_in, port)
        if ipv4.srcip in sales_subnet.values() and ipv4.dstip in it_subnet.values():
          port = 3
          print("going to switch 3")
          self.accept(packet, packet_in, port)
        if ipv4.srcip in ot_subnet.values() and ipv4.dstip in sales_subnet.values():
          port = 1
          print("going to switch 1")
          self.accept(packet, packet_in, port)
        if ipv4.srcip in ot_subnet.values() and ipv4.dstip in it_subnet.values():
          port = 3
          print("going to switch 3")
          self.accept(packet, packet_in, port)
        if ipv4.srcip in it_subnet.values() and ipv4.dstip in sales_subnet.values():
          port = 1
          print("going to switch 1")
          self.accept(packet, packet_in, port)
        if ipv4.srcip in it_subnet.values() and ipv4.dstip in ot_subnet.values():
          port = 2
          print("going to switch 2")
          self.accept(packet, packet_in, port)
      
      #sales Switch
      if switch_id == 1:
        print("Switch 1")
        #Within the Sales Department
        if ipv4.dstip in sales_subnet.values():
          print("ipv4 in Sales")
          if ipv4.dstip == '200.20.2.8':
            print("ipv4 == 200.20.2.8")
            port = 2
            self.accept(packet, packet_in, port)
          if ipv4.dstip == '200.20.2.10':
            print("ipv4 == 200.20.2.10")
            port = 1
            self.accept(packet, packet_in, port)
          if ipv4.dstip == '200.20.2.9':
            print("ipv4 == 200.20.2.9")
            port = 7
            self.accept(packet, packet_in, port)


        #to the OT Department or IT Department
        if ipv4.dstip in ot_subnet.values() or ipv4.dstip in it_subnet.values():
          port = 5
          self.accept(packet, packet_in, port)

      if switch_id == 3:
        print("Switch 3")
        #within the IT Department
        if ipv4.dstip in it_subnet.values():
          if ipv4.dstip == '200.20.4.7':
            port = 2
            self.accept(packet, packet_in, port)
          if ipv4.dstip == '200.20.4.6':
            port = 1
            self.accept(packet, packet_in, port)
        #to the sales Department or OT Department
        if ipv4.dstip in sales_subnet.values() or ipv4.dstip in ot_subnet.values():
          port = 7
          self.accept(packet, packet_in, port)
      

#within the OT department
      if switch_id == 2:
        print("Switch 2")
        if ipv4.dstip in ot_subnet.values():
          print("ipv4 in OT")
          if ipv4.dstip == '200.20.3.5':
            print("ipv4=200.20.3.5")
            port = 2
            self.accept(packet, packet_in, port)
          if ipv4.dstip == '200.20.3.4':
            port = 1
            print("ipv4=200.20.3.4")
            self.accept(packet, packet_in, port)
        if ipv4.dstip in it_subnet.values() or ipv4.dstip in sales_subnet.values():
          port = 6
          self.accept(packet, packet_in, port)

    if tcp:
      ipv4 = packet.find("ipv4")
      if switch_id == 5:
        print("SWITCH 5")
        print(ipv4.srcip)
        print(ipv4.dstip)
        if ipv4.srcip in data_subnet.values() and ipv4.dstip in ot_subnet.values():
          port = 2
          print("going to switch 2")
          self.accept(packet, packet_in, port)
        if ipv4.srcip in data_subnet.values() and ipv4.dstip in it_subnet.values():
          port = 3
          print("going to switch 3")
          self.accept(packet, packet_in, port)
        if ipv4.srcip in ot_subnet.values() and ipv4.dstip in data_subnet.values():
          port = 4
          print("going to switch 4")
          self.accept(packet, packet_in, port)
        if ipv4.srcip in ot_subnet.values() and ipv4.dstip in it_subnet.values():
          port = 3
          print("going to switch 3")
          self.accept(packet, packet_in, port)
        if ipv4.srcip in it_subnet.values() and ipv4.dstip in data_subnet.values():
          port = 4
          print("going to switch 4")
          self.accept(packet, packet_in, port)
        if ipv4.srcip in it_subnet.values() and ipv4.dstip in ot_subnet.values():
          port = 2
          print("going to switch 2")
          self.accept(packet, packet_in, port)
      

      #Within the Data Department
      #Data Switch
      if switch_id == 4:
        print("Switch 4")
        if ipv4.dstip in data_subnet.values():
          print("ipv4 in data")
          if ipv4.dstip == '200.20.1.1':
            print("ipv4 == 200.20.1.1")
            port = 1
            self.accept(packet, packet_in, port)
          if ipv4.dstip == '200.20.1.2':
            print("ipv4 == 200.20.1.2")
            port = 2
            self.accept(packet, packet_in, port)
          if ipv4.dstip == '200.20.1.3':
            print("ipv4 == 200.20.1.3")
            port = 3
            self.accept(packet, packet_in, port)
        #to the OT Department or IT Department
        if ipv4.dstip in ot_subnet.values() or ipv4.dstip in it_subnet.values():
          port = 8
          self.accept(packet, packet_in, port)

      #within the IT Department 
      if switch_id == 3:
        print("Switch 3")
        if ipv4.dstip in it_subnet.values():
          if ipv4.dstip == '200.20.4.7':
            port = 2
            self.accept(packet, packet_in, port)
          if ipv4.dstip == '200.20.4.6':
            port = 1
            self.accept(packet, packet_in, port)
        #to the sales Department or OT Department
        if ipv4.dstip in data_subnet.values() or ipv4.dstip in ot_subnet.values():
          port = 7
          self.accept(packet, packet_in, port)

      #within the OT department
      if switch_id == 2:
        print("Switch 2")
        if ipv4.dstip in ot_subnet.values():
          print("ipv4 in OT")
          if ipv4.dstip == '200.20.3.5':
            print("ipv4=200.20.3.5")
            port = 2
            self.accept(packet, packet_in, port)
          if ipv4.dstip == '200.20.3.4':
            port = 1
            print("ipv4=200.20.3.4")
            self.accept(packet, packet_in, port)
        if ipv4.dstip in it_subnet.values() or ipv4.dstip in data_subnet.values():
          port = 6
          self.accept(packet, packet_in, port)

    if udp:
      ipv4 = packet.find("ipv4")
      if switch_id == 5:
        print("SWITCH 5")
        print(ipv4.srcip)
        print(ipv4.dstip)
        if ipv4.srcip in sales_subnet.values() and ipv4.dstip in data_subnet.values():
          port = 4
          print("going to switch 4")
          self.accept(packet, packet_in, port)
        if ipv4.srcip in data_subnet.values() and ipv4.dstip in sales_subnet.values():
          port = 1
          print("going to switch 1")
          self.accept(packet, packet_in, port)
        if ipv4.srcip in sales_subnet.values() and ipv4.dstip in sales_subnet.values():
          port = 1
          print("going to switch 1")
          self.accept(packet, packet_in, port)
        if ipv4.srcip in data_subnet.values() and ipv4.dstip in data_subnet.values():
          port = 4
          print("going to switch 4")
          self.accept(packet, packet_in, port)
          
       #Within the Data Department    
      if switch_id == 4:
        print("Switch 4")
        if ipv4.dstip in data_subnet.values():
          print("ipv4 in data")
          if ipv4.dstip == '200.20.1.1':
            print("ipv4 == 200.20.1.1")
            port = 1
            self.accept(packet, packet_in, port)
          if ipv4.dstip == '200.20.1.2':
            print("ipv4 == 200.20.1.2")
            port = 2
            self.accept(packet, packet_in, port)
          if ipv4.dstip == '200.20.1.3':
            print("ipv4 == 200.20.1.3")
            port = 3
            self.accept(packet, packet_in, port)
        #to the OT Department or IT Department
        if ipv4.dstip in sales_subnet.values():
          port = 8
          self.accept(packet, packet_in, port)


       #Within the Data Department
      if switch_id == 1:
        print("Switch 1")
        if ipv4.dstip in sales_subnet.values():
          print("ipv4 in data")
          if ipv4.dstip == '200.20.2.8':
            print("ipv4 == 200.20.2.8")
            port = 1
            self.accept(packet, packet_in, port)
          if ipv4.dstip == '200.20.2.10':
            print("ipv4 == 200.20.2.10")
            port = 2
            self.accept(packet, packet_in, port)
          if ipv4.dstip == '200.20.2.9':
            print("ipv4 == 200.20.2.9")
            port = 3
            self.accept(packet, packet_in, port)
        #to the OT Department or IT Department
        if ipv4.dstip in data_subnet.values():
          port = 5
          self.accept(packet, packet_in, port)

  
  def accept (self, packet, packet_in, portkp):

    msg = of.ofp_flow_mod()
    msg.match = of.ofp_match.from_packet(packet)
    msg.actions.append(of.ofp_action_output(port = portkp))
    msg.data = packet_in
    self.connection.send(msg)

  def drop (self, packet):

    msg = of.ofp_flow_mod()
    msg.match = of.ofp_match.from_packet(packet)
    msg.idle_timeout = 30
    msg.hard_timeout = 30
    self.connection.send(msg)


  def _handle_PacketIn (self, event):
    """
    Handles packet in messages from the switch.
    """
    packet = event.parsed # This is the parsed packet data.
    if not packet.parsed:
      log.warning("Ignoring incomplete packet")
      return

    packet_in = event.ofp # The actual ofp_packet_in message.
    self.do_routing(packet, packet_in, event.port, event.dpid)

def launch ():
  """
  Starts the component
  """
  def start_switch (event):
    log.debug("Controlling %s" % (event.connection,))
    Routing(event.connection)
  core.openflow.addListenerByName("ConnectionUp", start_switch)
